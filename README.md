# ActiveMQServer

### 项目介绍

在 Eclipse 中启动 ActiveMQ 的服务端


### 软件架构

ActiveMQ 版本： 5.15.6

Eclipse 版本： 4.9.0

[ActiveMQ 二进制安装包](http://www.apache.org/dyn/closer.cgi?filename=/activemq/5.15.6/apache-activemq-5.15.6-bin.zip&action=download)

[ActiveMQ 源码](http://www.apache.org/dyn/closer.cgi?path=/activemq/5.15.6/activemq-parent-5.15.6-source-release.zip)


### 安装教程

1. 在 Eclipse 的 workspace 中创建一个名为 `ActiveMQServer` 的“Java Project”，JRE 选择“Use an execution environment JRE: JavaSE-1.8”
2. 在 `src` 目录下，创建一个“Java Class”源代码文件 `Main.java`，class name 为 `Main`，package 为 `org.apache.activemq.console`
3. `Main.java` 的代码，从 ActiveMQ 的源码复制过来即可，路径是 `/activemq-parent-5.15.6/activemq-console/src/main/java/org/apache/activemq/console/Main.java`
4. 在本 GIT 仓库的 `launch_config` 目录下，有两个 Launch Configuration 文件，可以通过 Eclipse 的 Import 功能导入到当前 workspace 中
5. 将 ActiveMQ 二进制安装包中的 `conf`、`data`、`lib`、`webapps` 四个目录复制到 Eclipse 的 `ActiveMQServer` 项目目录下
6. 删除 Eclipse 项目目录中 `conf` 子目录下的 `activemq.xml`、`broker.ks`、`broker.ts`、`broker-localhost.cert`、`client.ks`、`client.ts`
7. 在本 GIT 仓库的 `eclipse_proj/conf` 目录中，将 `activemq.xml`、`client.keystore`、`server.keystore` 复制到第 6 步的 `conf` 目录下（`client.keystore`、`server.keystore` 两个文件来自 ActiveMQ 源码中的 `/activemq-parent-5.15.6/activemq-http/src/test/resources` 目录）


### 使用说明

#### 启动

1. 在 Eclipse 的“Package Explorer”中，右击 `ActiveMQServer`，选择“Open Project”
2. 在 Eclipse 的“Package Explorer”中，右击 `ActiveMQServer`，选择“Run As -> Run Configurations...”
3. 在“Run Configurations”窗口的左侧，选择“Java Application -> ActiveMQServerStart”
4. 点击“Run Configurations”窗口的右下角的“Run”按钮，查看 Eclipse 的“Console”面板


#### 停止

关闭服务端是通过 JMX 来通知服务端 JVM 的。

1. 在 Eclipse 的“Package Explorer”面板中，右击 `ActiveMQServer`，选择“Run As -> Run Configurations...”
2. 在“Run Configurations”窗口的左侧，选择“Java Application -> ActiveMQServerStop”
3. 点击“Run Configurations”窗口的右下角的“Run”按钮，查看 Eclipse 的“Console”面板


#### 清理

ActiveMQ 的服务端在运行时，会在 `ActiveMQServer` 项目目录下的 `data` 中生成日志等文件，删除该目录下的所有文件，只保留一个空的 `activemq.log` 文件就可以了。
